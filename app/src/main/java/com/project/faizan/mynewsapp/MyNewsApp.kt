package com.project.faizan.mynewsapp

import android.app.Application
import com.project.faizan.mynewsapp.data.ConnectivityInterceptor
import com.project.faizan.mynewsapp.data.ConnectivityInterceptorImpl
import com.project.faizan.mynewsapp.data.network.AppExecutors
import com.project.faizan.mynewsapp.data.network.MyNewsApiService
import com.project.faizan.mynewsapp.data.repository.NewsRepository
import com.project.faizan.mynewsapp.data.repository.NewsRepositoryImpl
import com.project.faizan.mynewsapp.ui.HomeViewModelFactory
import com.project.faizan.mynewsapp.ui.entertainment.EntertainmentViewModelFactory
import com.project.faizan.mynewsapp.ui.search.SearchViewModelFactory
import com.project.faizan.mynewsapp.ui.sports.SportsViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class MyNewsApp : Application(), KodeinAware {
    //kodien for dependency injection so it should be kodinAware

    override val kodein = Kodein.lazy {
        //androidXModule provide us with instances of context, services and anything that is related to android
        //so that we dont need to reinvent the wheel
        import(androidXModule(this@MyNewsApp))

        bind() from singleton { AppExecutors() }

        //bind connectivity interceptor as well as its underlying implementation
        bind<ConnectivityInterceptor>() with singleton {
            ConnectivityInterceptorImpl(
                instance()
            )
        }

        //bind api service
        //MyNewsApiService api service needs connectivity interceptor but we are passing instance here
        //because kodein will automatically get the connectivity interceptor from above binding
        bind() from singleton { MyNewsApiService(instance()) }

        bind<NewsRepository>() with singleton {
            NewsRepositoryImpl(
                instance(), instance()
            )
        }


        bind() from provider { HomeViewModelFactory(instance()) }
        bind() from provider { SportsViewModelFactory(instance()) }
        bind() from provider { EntertainmentViewModelFactory(instance()) }
        bind() from provider { SearchViewModelFactory(instance()) }

    }
}