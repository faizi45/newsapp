package com.project.faizan.mynewsapp.ui.international

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.project.faizan.mynewsapp.R
import com.project.faizan.mynewsapp.ScopedFragment
import com.project.faizan.mynewsapp.databinding.FragmentInternationalBinding

class InternationalFragment : ScopedFragment() {

    private lateinit var binding: FragmentInternationalBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_international, container, false
        )

        return binding.root
    }
}