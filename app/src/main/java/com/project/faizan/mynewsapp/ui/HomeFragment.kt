package com.project.faizan.mynewsapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.project.faizan.mynewsapp.R
import com.project.faizan.mynewsapp.ScopedFragment
import com.project.faizan.mynewsapp.data.network.Status
import com.project.faizan.mynewsapp.databinding.FragmentHomeBinding
import com.project.faizan.mynewsapp.internals.Logger
import com.project.faizan.mynewsapp.internals.SpacesItemDecoration
import com.project.faizan.mynewsapp.models.TodaysDto
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import org.kodein.di.generic.instance

class HomeFragment : ScopedFragment(), KodeinAware {

    private val TAG: String? = HomeFragment::class.java.simpleName

    override val kodein by closestKodein()

    private val viewModelFactory: HomeViewModelFactory by instance()

    private lateinit var viewModel: HomeViewModel

    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_home, container, false
        )
        binding.lifecycleOwner = this

//        initGUI()

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)
        getHomeData()
    }

    private fun getHomeData() {

        Logger.log("getHomeData():")

        viewModel.todaysNewsListLD.observe(this, Observer {
            if (it.status == Status.SUCCESS || it.data != null)
                renderSections(it.data!!)
        })
    }

    private fun renderSections(data: List<TodaysDto>) {
        val mRecyclerView = binding.rvTday

        mRecyclerView?.layoutManager = LinearLayoutManager(activity?.applicationContext, RecyclerView.VERTICAL, false)
        mRecyclerView?.addItemDecoration(SpacesItemDecoration(20))
        mRecyclerView?.let { ViewCompat.setNestedScrollingEnabled(it, false) }

        val groupAdapter = GroupAdapter<ViewHolder>().apply {

            addAll(data.toTodayNewsItem())

        }

        mRecyclerView.apply {

            this?.layoutManager =
                LinearLayoutManager(this@HomeFragment.context, RecyclerView.VERTICAL, false)
            this?.adapter = groupAdapter

        }
    }


    private fun List<TodaysDto>.toTodayNewsItem(): List<TodayNewsItem> {
        return this.map {
            TodayNewsItem(it)
        }
    }
}