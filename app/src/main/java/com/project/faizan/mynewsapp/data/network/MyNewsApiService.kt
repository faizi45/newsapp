package com.project.faizan.mynewsapp.data.network

import android.util.Log
import androidx.lifecycle.LiveData
import com.project.faizan.mynewsapp.data.ConnectivityInterceptor
import com.project.faizan.mynewsapp.models.TodayNewsResponseDto
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

const val API_KEY = "666ee64364b7482a8e789fd9d3193033"

interface MyNewsApiService {

    /**
    This is a sample post request.

    @POST("login/registerDevice")
    fun registerDeiceApi(
    @Header("app") app: String,
    @Header("deviceId") deviceId: String
    ): LiveData<ApiResponse<RegisterDeviceResponseDto>>
     **/

    @GET("top-headlines")
    fun getTopHeadlines(
        @Query("country") country: String
    ): LiveData<ApiResponse<TodayNewsResponseDto>>

    @GET("everything")
    fun getEverythingApi(
        @Query("q") q: String
    ): LiveData<ApiResponse<TodayNewsResponseDto>>

    companion object {

        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor
        ): MyNewsApiService {

            val requestInterceptorChain = Interceptor { chain ->

                val url = chain.request()
                    .url()
                    .newBuilder()
                    .addQueryParameter("apiKey", API_KEY)
                    .build()

                val request = chain.request()
                    .newBuilder()
                    .url(url)
                    .build()

                Log.d("RAW_URL", url.toString())

                return@Interceptor chain.proceed(request)
            }


            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(requestInterceptorChain)
                .addInterceptor(connectivityInterceptor)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://newsapi.org/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .build()
                .create(MyNewsApiService::class.java)


        }
    }
}