package com.project.faizan.mynewsapp.ui.entertainment

import androidx.lifecycle.LiveData
import com.project.faizan.mynewsapp.BaseViewModel
import com.project.faizan.mynewsapp.data.network.Resource
import com.project.faizan.mynewsapp.data.repository.NewsRepository
import com.project.faizan.mynewsapp.internals.Logger
import com.project.faizan.mynewsapp.models.TodaysDto

class EntertainmentViewModel(
    val repository: NewsRepository
) : BaseViewModel() {

    private val TAG: String = EntertainmentViewModel::class.java.simpleName.toString()

    var cricketNewsListLD: LiveData<Resource<List<TodaysDto>>>

    val q: String = "entertainment"

    init {
        Logger.log(TAG, "init:")

        cricketNewsListLD = repository.getEntertainmentNews(q)

    }

}