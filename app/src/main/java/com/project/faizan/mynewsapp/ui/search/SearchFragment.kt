package com.project.faizan.mynewsapp.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.project.faizan.mynewsapp.R
import com.project.faizan.mynewsapp.ScopedFragment
import com.project.faizan.mynewsapp.data.network.Status
import com.project.faizan.mynewsapp.databinding.FragmentSearchBinding
import com.project.faizan.mynewsapp.internals.SpacesItemDecoration
import com.project.faizan.mynewsapp.models.TodaysDto
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_search.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import org.kodein.di.generic.instance

class SearchFragment : ScopedFragment(), KodeinAware {

    private val TAG: String? = SearchFragment::class.java.simpleName

    override val kodein by closestKodein()

    private val viewModelFactory: SearchViewModelFactory by instance()

    private lateinit var viewModel: SearchViewModel

    private lateinit var binding: FragmentSearchBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_search, container, false
        )
        binding.lifecycleOwner = this

//        initGUI()

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SearchViewModel::class.java)

        listeners()
//        getHomeData()
    }

    private fun listeners() {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                avLoader.visibility = View.VISIBLE
                viewModel.onSearchClick(query)

                viewModel.searchNewsListLD.observe(this@SearchFragment, Observer {
                    if (it.status == Status.SUCCESS || !it.data.isNullOrEmpty())
                        renderSections(it.data!!)
                    else
                        tvEmptyMessage.visibility = View.VISIBLE
                })
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })

    }

    private fun renderSections(data: List<TodaysDto>) {
        avLoader.visibility = View.GONE
        tvEmptyMessage.visibility = View.GONE

        val mRecyclerView = binding.recyclerView

        mRecyclerView?.layoutManager = LinearLayoutManager(activity?.applicationContext, RecyclerView.VERTICAL, false)
        mRecyclerView?.addItemDecoration(SpacesItemDecoration(5))
        mRecyclerView?.let { ViewCompat.setNestedScrollingEnabled(it, false) }

        val groupAdapter = GroupAdapter<ViewHolder>().apply {

            addAll(data.toTodayNewsItem())

        }

        mRecyclerView.apply {

            this?.layoutManager =
                LinearLayoutManager(this@SearchFragment.context, RecyclerView.VERTICAL, false)
            this?.adapter = groupAdapter

        }
    }


    private fun List<TodaysDto>.toTodayNewsItem(): List<SearchItem> {
        return this.map {
            SearchItem(it)
        }
    }
}