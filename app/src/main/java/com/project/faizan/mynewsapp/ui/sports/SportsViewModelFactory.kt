package com.project.faizan.mynewsapp.ui.sports

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.project.faizan.mynewsapp.data.repository.NewsRepository

class SportsViewModelFactory(
    private val repository: NewsRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SportsViewModel(repository) as T
    }
}