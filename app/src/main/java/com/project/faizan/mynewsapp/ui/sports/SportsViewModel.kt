package com.project.faizan.mynewsapp.ui.sports

import androidx.lifecycle.LiveData
import com.project.faizan.mynewsapp.BaseViewModel
import com.project.faizan.mynewsapp.data.network.Resource
import com.project.faizan.mynewsapp.data.repository.NewsRepository
import com.project.faizan.mynewsapp.internals.Logger
import com.project.faizan.mynewsapp.models.TodaysDto

class SportsViewModel(
    val repository: NewsRepository
) : BaseViewModel() {
    private val TAG: String = SportsViewModel::class.java.simpleName.toString()

    var cricketNewsListLD: LiveData<Resource<List<TodaysDto>>>
    var footballNewsListLD: LiveData<Resource<List<TodaysDto>>>
    var tennisNewsListLD: LiveData<Resource<List<TodaysDto>>>


    val q: String = "cricket"

    init {
        Logger.log(TAG, "init:")

        cricketNewsListLD = repository.getCricketNews(q)

        footballNewsListLD = repository.getFootballNews("football")

        tennisNewsListLD = repository.getTennisNews("tennis")
    }
}