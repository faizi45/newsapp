package com.project.faizan.mynewsapp.utils.dialog;

/**
 * Created by Hamza Mehmood on 4/2/2018.
 */

public interface GeneralDialogListener {

    void onPositiveButtonPressed();

    void onNegativeButtonPressed();

}
