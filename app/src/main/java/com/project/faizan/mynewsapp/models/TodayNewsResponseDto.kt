package com.project.faizan.mynewsapp.models

data class TodayNewsResponseDto(

    val status: String,
    val totalResults: Int,
    val articles: ArrayList<TodaysDto>? = null
)