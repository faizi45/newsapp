package com.project.faizan.mynewsapp.utils.dialog;

/**
 * Created by Hamza Mehmood on 4/2/2018.
 */

public interface ConfirmOrCancelDialogListener {

    void onPositiveButtonPressed();

}
