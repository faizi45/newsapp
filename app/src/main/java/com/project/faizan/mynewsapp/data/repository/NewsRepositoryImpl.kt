package com.project.faizan.mynewsapp.data.repository

import androidx.lifecycle.LiveData
import com.project.faizan.mynewsapp.data.AbsentLiveData
import com.project.faizan.mynewsapp.data.network.*
import com.project.faizan.mynewsapp.internals.Logger
import com.project.faizan.mynewsapp.models.TodayNewsResponseDto
import com.project.faizan.mynewsapp.models.TodaysDto

class NewsRepositoryImpl(

    private val appExecutorsJv: AppExecutors,
    private val apiService: MyNewsApiService
) : NewsRepository {

    private val TAG: String = NewsRepositoryImpl::class.java.simpleName

    override fun getTodaysNews(country: String): LiveData<Resource<List<TodaysDto>>> {
        Logger.log(TAG, "getTodaysNews()")

        return object : NetworkBoundResource<List<TodaysDto>, TodayNewsResponseDto>(appExecutorsJv) {

            var todaysDto: List<TodaysDto>? = null

            override fun saveCallResult(item: TodayNewsResponseDto) {
                Logger.log(TAG, "getTodaysNews: saveCallResult()")

                if (item.status.equals("ok", true) && item.articles != null)
                    todaysDto = item.articles
//                    userDao.upsert(item.respData)
            }

            override fun shouldFetch(data: List<TodaysDto>?): Boolean {
                Logger.log(TAG, "getTodaysNews: shouldFetch()")

                return true
            }

            override fun loadFromDb(): LiveData<List<TodaysDto>> {
                Logger.log(TAG, "getTodaysNews: loadFromDb()")
                return if (todaysDto == null) {
                    AbsentLiveData.create()
                } else {
                    object : LiveData<List<TodaysDto>>() {
                        override fun onActive() {
                            super.onActive()
                            value = todaysDto
                        }
                    }

                }
            }

            override fun createCall(): LiveData<ApiResponse<TodayNewsResponseDto>> {
                Logger.log(TAG, "getTodaysNews: createCall()")

                return apiService.getTopHeadlines(country)
            }
        }.asLiveData()
    }

    override fun getCricketNews(query: String): LiveData<Resource<List<TodaysDto>>> {
        Logger.log(TAG, "getCricketNews()")

        return object : NetworkBoundResource<List<TodaysDto>, TodayNewsResponseDto>(appExecutorsJv) {

            var todaysDto: List<TodaysDto>? = null

            override fun saveCallResult(item: TodayNewsResponseDto) {
                Logger.log(TAG, "getCricketNews: saveCallResult()")

                if (item.status.equals("ok", true) && item.articles != null)
                    todaysDto = item.articles
//                    userDao.upsert(item.respData)
            }

            override fun shouldFetch(data: List<TodaysDto>?): Boolean {
                Logger.log(TAG, "getCricketNews: shouldFetch()")

                return true
            }

            override fun loadFromDb(): LiveData<List<TodaysDto>> {
                Logger.log(TAG, "getCricketNews: loadFromDb()")
                return if (todaysDto == null) {
                    AbsentLiveData.create()
                } else {
                    object : LiveData<List<TodaysDto>>() {
                        override fun onActive() {
                            super.onActive()
                            value = todaysDto
                        }
                    }

                }
            }

            override fun createCall(): LiveData<ApiResponse<TodayNewsResponseDto>> {
                Logger.log(TAG, "getCricketNews: createCall()")

                return apiService.getEverythingApi(query)
            }
        }.asLiveData()
    }

    override fun getFootballNews(query: String): LiveData<Resource<List<TodaysDto>>> {
        Logger.log(TAG, "getFootballNews()")

        return object : NetworkBoundResource<List<TodaysDto>, TodayNewsResponseDto>(appExecutorsJv) {

            var todaysDto: List<TodaysDto>? = null

            override fun saveCallResult(item: TodayNewsResponseDto) {
                Logger.log(TAG, "getFootballNews: saveCallResult()")

                if (item.status.equals("ok", true) && item.articles != null)
                    todaysDto = item.articles
//                    userDao.upsert(item.respData)
            }

            override fun shouldFetch(data: List<TodaysDto>?): Boolean {
                Logger.log(TAG, "getFootballNews: shouldFetch()")

                return true
            }

            override fun loadFromDb(): LiveData<List<TodaysDto>> {
                Logger.log(TAG, "getFootballNews: loadFromDb()")
                return if (todaysDto == null) {
                    AbsentLiveData.create()
                } else {
                    object : LiveData<List<TodaysDto>>() {
                        override fun onActive() {
                            super.onActive()
                            value = todaysDto
                        }
                    }

                }
            }

            override fun createCall(): LiveData<ApiResponse<TodayNewsResponseDto>> {
                Logger.log(TAG, "getFootballNews: createCall()")

                return apiService.getEverythingApi(query)
            }
        }.asLiveData()
    }

    override fun getTennisNews(query: String): LiveData<Resource<List<TodaysDto>>> {
        Logger.log(TAG, "getTennisNews()")

        return object : NetworkBoundResource<List<TodaysDto>, TodayNewsResponseDto>(appExecutorsJv) {

            var todaysDto: List<TodaysDto>? = null

            override fun saveCallResult(item: TodayNewsResponseDto) {
                Logger.log(TAG, "getTennisNews: saveCallResult()")

                if (item.status.equals("ok", true) && item.articles != null)
                    todaysDto = item.articles
//                    userDao.upsert(item.respData)
            }

            override fun shouldFetch(data: List<TodaysDto>?): Boolean {
                Logger.log(TAG, "getTennisNews: shouldFetch()")

                return true
            }

            override fun loadFromDb(): LiveData<List<TodaysDto>> {
                Logger.log(TAG, "getTennisNews: loadFromDb()")
                return if (todaysDto == null) {
                    AbsentLiveData.create()
                } else {
                    object : LiveData<List<TodaysDto>>() {
                        override fun onActive() {
                            super.onActive()
                            value = todaysDto
                        }
                    }

                }
            }

            override fun createCall(): LiveData<ApiResponse<TodayNewsResponseDto>> {
                Logger.log(TAG, "getTennisNews: createCall()")

                return apiService.getEverythingApi(query)
            }
        }.asLiveData()

    }

    override fun getEntertainmentNews(query: String): LiveData<Resource<List<TodaysDto>>> {

        Logger.log(TAG, "getEntertainmentNews()")

        return object : NetworkBoundResource<List<TodaysDto>, TodayNewsResponseDto>(appExecutorsJv) {

            var todaysDto: List<TodaysDto>? = null

            override fun saveCallResult(item: TodayNewsResponseDto) {
                Logger.log(TAG, "getEntertainmentNews: saveCallResult()")

                if (item.status.equals("ok", true) && item.articles != null)
                    todaysDto = item.articles
//                    userDao.upsert(item.respData)
            }

            override fun shouldFetch(data: List<TodaysDto>?): Boolean {
                Logger.log(TAG, "getEntertainmentNews: shouldFetch()")

                return true
            }

            override fun loadFromDb(): LiveData<List<TodaysDto>> {
                Logger.log(TAG, "getEntertainmentNews: loadFromDb()")
                return if (todaysDto == null) {
                    AbsentLiveData.create()
                } else {
                    object : LiveData<List<TodaysDto>>() {
                        override fun onActive() {
                            super.onActive()
                            value = todaysDto
                        }
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<TodayNewsResponseDto>> {
                Logger.log(TAG, "getEntertainmentNews: createCall()")

                return apiService.getEverythingApi(query)
            }
        }.asLiveData()

    }

    override fun searchNews(query: String): LiveData<Resource<List<TodaysDto>>> {

        Logger.log(TAG, "searchNews()")

        return object : NetworkBoundResource<List<TodaysDto>, TodayNewsResponseDto>(appExecutorsJv) {

            var todaysDto: List<TodaysDto>? = null

            override fun saveCallResult(item: TodayNewsResponseDto) {
                Logger.log(TAG, "searchNews: saveCallResult()")

                if (item.status.equals("ok", true) && item.articles != null)
                    todaysDto = item.articles
//                    userDao.upsert(item.respData)
            }

            override fun shouldFetch(data: List<TodaysDto>?): Boolean {
                Logger.log(TAG, "searchNews: shouldFetch()")

                return true
            }

            override fun loadFromDb(): LiveData<List<TodaysDto>> {
                Logger.log(TAG, "searchNews: loadFromDb()")
                return if (todaysDto == null) {
                    AbsentLiveData.create()
                } else {
                    object : LiveData<List<TodaysDto>>() {
                        override fun onActive() {
                            super.onActive()
                            value = todaysDto
                        }
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<TodayNewsResponseDto>> {
                Logger.log(TAG, "searchNews: createCall()")

                return apiService.getEverythingApi(query)
            }
        }.asLiveData()

    }
}