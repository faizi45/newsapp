package com.project.faizan.mynewsapp.ui.entertainment

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.project.faizan.mynewsapp.data.repository.NewsRepository

class EntertainmentViewModelFactory(
    private val repository: NewsRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return EntertainmentViewModel(repository) as T
    }
}