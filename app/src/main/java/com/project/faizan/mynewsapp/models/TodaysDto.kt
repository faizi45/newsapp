package com.project.faizan.mynewsapp.models

data class TodaysDto(
    val source: sources,
    val author: String,
    val title: String,
    val description: String,
    val url: String,
    val urlToImage: String,
    val publishedAt: String,
    val content: String
)

data class sources(
    val id: String,
    val name: String
)

