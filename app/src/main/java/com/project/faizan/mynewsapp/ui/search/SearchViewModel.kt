package com.project.faizan.mynewsapp.ui.search

import androidx.lifecycle.LiveData
import com.project.faizan.mynewsapp.BaseViewModel
import com.project.faizan.mynewsapp.data.network.Resource
import com.project.faizan.mynewsapp.data.repository.NewsRepository
import com.project.faizan.mynewsapp.models.TodaysDto

class SearchViewModel(
    val repository: NewsRepository
) : BaseViewModel() {

    private val TAG: String = SearchViewModel::class.java.simpleName.toString()

    lateinit var searchNewsListLD: LiveData<Resource<List<TodaysDto>>>

    fun onSearchClick(query: String?) {
        searchNewsListLD = repository.searchNews(query!!)
    }
}