package com.project.faizan.mynewsapp.ui

import androidx.lifecycle.LiveData
import com.project.faizan.mynewsapp.BaseViewModel
import com.project.faizan.mynewsapp.data.network.Resource
import com.project.faizan.mynewsapp.data.repository.NewsRepository
import com.project.faizan.mynewsapp.internals.Logger
import com.project.faizan.mynewsapp.models.TodaysDto

class HomeViewModel(
    val repository: NewsRepository
) : BaseViewModel() {

    private val TAG: String = HomeViewModel::class.java.simpleName.toString()

    var todaysNewsListLD: LiveData<Resource<List<TodaysDto>>>

    val country: String = "US"

    init {
        Logger.log(TAG, "init:")

        todaysNewsListLD = repository.getTodaysNews(country)

    }
}