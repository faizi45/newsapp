package com.project.faizan.mynewsapp.data.repository

import androidx.lifecycle.LiveData
import com.project.faizan.mynewsapp.data.network.Resource
import com.project.faizan.mynewsapp.models.TodaysDto

interface NewsRepository {

    fun getTodaysNews(country: String): LiveData<Resource<List<TodaysDto>>>

    fun getCricketNews(query: String): LiveData<Resource<List<TodaysDto>>>

    fun getFootballNews(query: String): LiveData<Resource<List<TodaysDto>>>

    fun getTennisNews(query: String): LiveData<Resource<List<TodaysDto>>>

    fun getEntertainmentNews(query: String): LiveData<Resource<List<TodaysDto>>>

    fun searchNews(query: String): LiveData<Resource<List<TodaysDto>>>

}