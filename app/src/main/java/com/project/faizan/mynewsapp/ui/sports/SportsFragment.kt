package com.project.faizan.mynewsapp.ui.sports

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.project.faizan.mynewsapp.R
import com.project.faizan.mynewsapp.ScopedFragment
import com.project.faizan.mynewsapp.data.network.Status
import com.project.faizan.mynewsapp.databinding.FragmentGamesBinding
import com.project.faizan.mynewsapp.internals.Logger
import com.project.faizan.mynewsapp.internals.SpacesItemDecoration
import com.project.faizan.mynewsapp.models.TodaysDto
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import org.kodein.di.generic.instance

class SportsFragment : ScopedFragment(), KodeinAware {

    private val TAG: String? = SportsFragment::class.java.simpleName

    override val kodein by closestKodein()

    private val viewModelFactory: SportsViewModelFactory by instance()

    private lateinit var viewModel: SportsViewModel

    private lateinit var binding: FragmentGamesBinding
    private var inflater: LayoutInflater? = null
    private var sectionName: TextView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_games, container, false
        )
        binding.lifecycleOwner = this

//        initGUI()

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SportsViewModel::class.java)
        inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        getData()
    }

    private fun getData() {

        Logger.log("getData():")

        viewModel.cricketNewsListLD.observe(this, Observer {
            if (it.status == Status.SUCCESS || it.data != null)
                render("Cricket", it.data!!)
        })

        viewModel.footballNewsListLD.observe(this, Observer {
            if (it.status == Status.SUCCESS || it.data != null)
                render("Football", it.data!!)
        })

        viewModel.tennisNewsListLD.observe(this, Observer {
            if (it.status == Status.SUCCESS || it.data != null)
                render("Tennis", it.data!!)
        })
    }

/*

    private fun renderSections(data: List<TodaysDto>) {
        val mRecyclerView = binding.rvTday

        mRecyclerView?.layoutManager = LinearLayoutManager(activity?.applicationContext, RecyclerView.HORIZONTAL, false)
        mRecyclerView?.addItemDecoration(SpacesItemDecoration(20))
        mRecyclerView?.let { ViewCompat.setNestedScrollingEnabled(it, false) }

        val groupAdapter = GroupAdapter<ViewHolder>().apply {

            addAll(data.toCricketNewsItem())

        }

        mRecyclerView.apply {

            this?.layoutManager =
                LinearLayoutManager(this@SportsFragment.context, RecyclerView.HORIZONTAL, false)
            this?.adapter = groupAdapter

        }
    }
*/

    private fun render(type: String, data: List<TodaysDto>) {

        val view = inflater?.inflate(R.layout.item_recyclerview, null)

        val mRecyclerView = view?.findViewById<RecyclerView>(R.id.recyclerView)

        sectionName = view?.findViewById(R.id.tvSectionName)
        sectionName?.text = type

        mRecyclerView?.layoutManager = LinearLayoutManager(activity?.applicationContext, RecyclerView.HORIZONTAL, false)
        mRecyclerView?.addItemDecoration(SpacesItemDecoration(20))
        mRecyclerView?.let { ViewCompat.setNestedScrollingEnabled(it, false) }

        val groupAdapter = GroupAdapter<ViewHolder>().apply {

            addAll(data.toCricketNewsItem())

        }

        mRecyclerView.apply {

            this?.layoutManager =
                LinearLayoutManager(this@SportsFragment.context, RecyclerView.HORIZONTAL, false)
            this?.adapter = groupAdapter

        }
        binding.userContentContainer.addView(view)
        binding.userContentContainer.invalidate()


    }

    private fun List<TodaysDto>.toCricketNewsItem(): List<SportsItem> {
        return this.map {
            SportsItem(it)
        }
    }
}