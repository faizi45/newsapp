package com.project.faizan.mynewsapp.internals

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class NewsGlideModule : AppGlideModule() {
}