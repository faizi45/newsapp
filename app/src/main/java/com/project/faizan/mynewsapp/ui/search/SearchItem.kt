package com.project.faizan.mynewsapp.ui.search

import com.project.faizan.mynewsapp.R
import com.project.faizan.mynewsapp.internals.GlideApp
import com.project.faizan.mynewsapp.models.TodaysDto
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_search.view.*

class SearchItem(val items: TodaysDto) : Item() {
    override fun bind(viewHolder: ViewHolder, position: Int) {

        viewHolder.apply {

            itemView.tvTitle.text = items.title
            itemView.tvDescription.text = items.description

            updateImage()
        }

    }

    override fun getLayout() = R.layout.item_search


    private fun ViewHolder.updateImage() {
        GlideApp.with(this.containerView)
            .load(items.urlToImage)
            .error(R.drawable.news_bg)
            .placeholder(R.drawable.news_bg)
            .into(itemView.ivAudioThumb)

    }
}