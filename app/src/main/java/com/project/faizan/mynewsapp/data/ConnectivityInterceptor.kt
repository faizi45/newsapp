package com.project.faizan.mynewsapp.data

import okhttp3.Interceptor

interface ConnectivityInterceptor : Interceptor {
//used for dependency injection via kodein
}